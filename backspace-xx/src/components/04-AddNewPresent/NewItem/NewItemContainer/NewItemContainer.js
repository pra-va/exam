import React, { Component } from "react";
import NewItemPresentation from "./../NewItemPresentation/NewItemPresentation";
import axios from "axios";

class NewItemContainer extends Component {
  constructor() {
    super();
    this.state = {
      description: "",
      name: "",
      pictureId: "",
      presentType: "",
      targetAudiance: ""
    };
  }

  nameChangeHandler = event => {
    event.preventDefault();
    this.setState({
      name: event.target.value
    });
  };

  pictureIdCHangeHandler = event => {
    event.preventDefault();
    this.setState({
      pictureId: event.target.value
    });
  };

  presentTypeChangeHandler = event => {
    event.preventDefault();
    this.setState({
      presentType: event.target.value
    });
  };

  targetAudianceChangeHandler = event => {
    event.preventDefault();
    this.setState({
      targetAudiance: event.target.value
    });
  };

  descriptionChangedHandler = event => {
    event.preventDefault();
    this.setState({
      description: event.target.value
    });
  };

  submitHandler = event => {
    event.preventDefault();
    axios
      .post("http://localhost:8081/backspace-aplikacija/api/present", {
        description: this.state.description,
        name: this.state.name,
        pictureId: this.state.pictureId,
        presentType: this.state.presentType,
        targetAudiance: this.state.targetAudiance
      })
      .then(() => {
        this.props.history.push("/");
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    return (
      <div>
        <NewItemPresentation
          submitHandler={this.submitHandler}
          nameChangeHandler={this.nameChangeHandler}
          titleChangedHandler={this.titleChangedHandler}
          pictureIdCHangeHandler={this.pictureIdCHangeHandler}
          presentTypeChangeHandler={this.presentTypeChangeHandler}
          targetAudianceChangeHandler={this.targetAudianceChangeHandler}
          descriptionChangedHandler={this.descriptionChangedHandler}
        />
      </div>
    );
  }
}

export default NewItemContainer;
