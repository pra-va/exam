import React from "react";

const NewItemPresentation = props => {
  return (
    <form onSubmit={props.submitHandler}>
      <div className="form-row">
        <div className="form-group col-md-6">
          <label htmlFor="name">Name</label>
          <input
            onChange={props.nameChangeHandler}
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={props.name}
            required
          />
        </div>
        <div className="form-group col-md-6">
          <label htmlFor="pictureId">Picture ID</label>
          <input
            onChange={props.pictureIdCHangeHandler}
            type="text"
            step="0.01"
            className="form-control"
            id="pictureId"
            name="pictureId"
            value={props.description}
            required
          />
        </div>
        <div className="form-group col-md-6">
          <label htmlFor="presentType">Present Type</label>
          <select
            id="presentType"
            name="presentType"
            onChange={props.presentTypeChangeHandler}
            className="form-control"
          >
            <option value="TOY">Toy</option>
            <option value="EVENT">Event</option>
            <option value="ANIMAL">Animal</option>
          </select>
        </div>
        <div className="form-group col-md-6">
          <label htmlFor="targettargetAudiance">Target Audiance</label>
          <select
            id="targettargetAudiance"
            name="presentType"
            onChange={props.targettargetAudianceChangeHandler}
            className="form-control"
          >
            <option value="CHILDREN">Children</option>
            <option value="ADULT">Adult</option>
          </select>
        </div>

        <div className="form-group col-md-12">
          <label htmlFor="description">Description</label>
          <textarea
            onChange={props.descriptionChangedHandler}
            type="number"
            step="0.01"
            className="form-control"
            id="description"
            name="description"
            value={props.description}
            required
          />
        </div>

        <button type="submit" className="btn btn-light">
          SUBMIT
        </button>
      </div>
    </form>
  );
};

export default NewItemPresentation;
