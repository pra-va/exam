import React from "react";
import Form from "./FormPresentation/FormPresentation";

const NewItemPresentation = props => {
  return (
    <div className="row">
      <div className="col-12 ">
        <Form
          submitHandler={props.submitHandler}
          nameChangeHandler={props.nameChangeHandler}
          titleChangedHandler={props.titleChangedHandler}
          pictureIdCHangeHandler={props.pictureIdCHangeHandler}
          presentTypeChangeHandler={props.presentTypeChangeHandler}
          targettargetAudianceChangeHandler={props.targettargetAudianceChangeHandler}
          descriptionChangedHandler={props.descriptionChangedHandler}
        />
      </div>
    </div>
  );
};

export default NewItemPresentation;
