import React, { Component } from "react";
import Card from "./../Card/CardPresentationComponent";
import axios from "axios";
import pic1 from "./../../../resources/1.jpg";
import pic2 from "./../../../resources/2.jpg";

class CardsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { presents: [], images: [pic1, pic2] };

    axios
      .get("http://localhost:8081/backspace-aplikacija/api/present")
      .then(response => {
        this.setState({ presents: response.data });
        console.log(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    var presents = this.state.presents.map((item, index) => {
      return (
        <div key={index} className="col-12 col-md-6 col-lg-4 mb-4">
          <Card
            key={item.id}
            image={this.state.images[item.pictureId]}
            name={item.name}
            targetAudiance={item.targetAudiance}
            presentType={item.presentType}
            description={item.description}
          />
        </div>
      );
    });
    return <div className="row">{presents}</div>;
  }
}

export default CardsContainer;
