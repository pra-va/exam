import React from "react";
import CardsContainer from "./CardList/CardsContainer";

var HomeComponent = () => {
  return (
    <div className="container">
      <CardsContainer />
    </div>
  );
};

export default HomeComponent;
