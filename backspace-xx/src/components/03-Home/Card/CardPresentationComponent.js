import React from "react";

var ProductCard = props => {
  return (
    <div className="">
      <div className="card text-white bg-secondary border-dark">
        <img src={props.image} className="card-img-top" alt="Unable to load" />
        <div className="card-body">
          <h4 className="card-title text-white">{props.name}</h4>
          <h5 className="card-title">Description: {props.description}</h5>
          <h5 className="card-title">Audiance: {props.targetAudiance}</h5>
          <h5 className="card-title">Type: {props.presentType}</h5>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
