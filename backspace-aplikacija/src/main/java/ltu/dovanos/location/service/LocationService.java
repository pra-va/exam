package ltu.dovanos.location.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ltu.dovanos.location.dao.LocationRepository;
import ltu.dovanos.location.model.Location;

@Service
public class LocationService {

	@Autowired
	private LocationRepository locationRepository;

	@Transactional
	public void createLocation(LocationData locationData) {
		this.locationRepository.save(new Location(locationData.getCountry(), locationData.getTown()));
	}
}
