package ltu.dovanos.location.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import ltu.dovanos.location.model.CreateLocaitonCommand;
import ltu.dovanos.location.service.LocationData;
import ltu.dovanos.location.service.LocationService;

@RestController
public class LocationController {

	@Autowired
	private LocationService locationService;

	@PostMapping
	@ApiOperation(value = "createLocaiton")
	public void createLocation(@RequestBody CreateLocaitonCommand command) {
		this.locationService.createLocation(new LocationData(command.getCountry(), command.getTown()));
	}
}
