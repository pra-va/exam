package ltu.dovanos.location.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ltu.dovanos.location.model.Location;

public interface LocationRepository extends JpaRepository<Location, String> {
}
