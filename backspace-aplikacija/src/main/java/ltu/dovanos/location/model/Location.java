package ltu.dovanos.location.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import ltu.dovanos.present.model.Present;

@Entity(name = "Location")
@Table(name = "location")
public class Location {
	@Id
	private String countryAndTown;
	private String country;
	private String town;

	@ManyToMany(mappedBy = "locations", cascade = { CascadeType.MERGE, CascadeType.DETACH }, fetch = FetchType.EAGER)
	private List<Present> presents = new ArrayList<Present>();

	public Location() {
	}

	public Location(String country, String town) {
		this.countryAndTown = country + "-" + town;
		this.country = country;
		this.town = town;
	}

	public void removePresent(Present present) {
		this.presents.remove(present);
	}

	public void addPresent(Present present) {
		this.presents.add(present);
	}

	public String getCountryAndTown() {
		return countryAndTown;
	}

	public void setCountryAndTown(String countryAndTown) {
		this.countryAndTown = countryAndTown;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public List<Present> getPresents() {
		return presents;
	}

	public void setPresents(List<Present> presents) {
		this.presents = presents;
	}

}
