package ltu.dovanos.location.model;

public class CreateLocaitonCommand {
	private String country;
	private String town;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

}
