package ltu.dovanos.present.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ltu.dovanos.present.model.Present;

public interface PresentRepository extends JpaRepository<Present, String> {
	Present findByName(String name);

	List<Present> myJoinQuery(String name);
}
