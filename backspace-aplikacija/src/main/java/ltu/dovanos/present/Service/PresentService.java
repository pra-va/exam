package ltu.dovanos.present.Service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ltu.dovanos.location.dao.LocationRepository;
import ltu.dovanos.location.model.Location;
import ltu.dovanos.present.dao.PresentRepository;
import ltu.dovanos.present.model.Present;

@Service
public class PresentService {

	@Autowired
	private PresentRepository presentRepository;

	@Autowired
	private LocationRepository locationRepository;

	@Transactional
	public PresentData getSinglePresent(String presentName) {
		Present present = this.presentRepository.findByName(presentName);
		return new PresentData(present.getName(), present.getDescription(), present.getPictureId(),
				present.getTargetAudiance(), present.getPresentType());
	}

	@Transactional
	public void addPresentsLocation(String locationName, String presentName) {
		Present present = this.presentRepository.findById(presentName).get();
		Location location = this.locationRepository.findById(locationName).get();

		if (present != null && location != null) {
			present.addLocation(location);
			location.addPresent(present);

			this.presentRepository.save(present);
			this.locationRepository.save(location);
		}

	}

	// Create
	@Transactional
	public void createPresent(PresentData presentData) {
		this.presentRepository.save(new Present(presentData.getName(), presentData.getDescription(),
				presentData.getPictureId(), presentData.getTargetAudiance(), presentData.getPresentType()));
	}

	// GetAll
	@Transactional
	public List<PresentData> getAllPresents() {
		return this.presentRepository
				.findAll().stream().map((present) -> new PresentData(present.getName(), present.getDescription(),
						present.getPictureId(), present.getTargetAudiance(), present.getPresentType()))
				.collect(Collectors.toList());
	}

	// Update
	@Transactional
	public void updatePresent(PresentData presentData, String presentName) {
		Present present = this.presentRepository.findById(presentName).get();
		this.presentRepository.delete(present);
		this.presentRepository.save(new Present(presentData.getName(), presentData.getDescription(),
				presentData.getPictureId(), presentData.getTargetAudiance(), presentData.getPresentType()));
	}
}
