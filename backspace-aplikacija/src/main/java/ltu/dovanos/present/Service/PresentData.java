package ltu.dovanos.present.Service;

import ltu.dovanos.present.model.PresentType;
import ltu.dovanos.present.model.TargetAutiance;

public class PresentData {
	private String name;
	private String description;
	private String pictureId;
	private TargetAutiance targetAudiance;
	private PresentType presentType;

	public PresentData() {
	}

	public PresentData(String name, String description, String pictureId, TargetAutiance targetAudiance,
			PresentType presentType) {
		this.name = name;
		this.description = description;
		this.pictureId = pictureId;
		this.targetAudiance = targetAudiance;
		this.presentType = presentType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPictureId() {
		return pictureId;
	}

	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}

	public TargetAutiance getTargetAudiance() {
		return targetAudiance;
	}

	public void setTargetAudiance(TargetAutiance targetAudiance) {
		this.targetAudiance = targetAudiance;
	}

	public PresentType getPresentType() {
		return presentType;
	}

	public void setPresentType(PresentType presentType) {
		this.presentType = presentType;
	}

}
