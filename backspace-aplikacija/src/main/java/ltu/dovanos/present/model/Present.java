package ltu.dovanos.present.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ltu.dovanos.location.model.Location;

@Entity(name = "Present")
@Table(name = "present")
@NamedQuery(name = "Present.myJoinQuery", query = "select p from Present p join p.locations l where p.name like ?1")
public class Present {
	@Id
	private String name;
	private String description;
	private String pictureId;
	@Enumerated
	private TargetAutiance targetAudiance;
	@Enumerated
	private PresentType presentType;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinTable(name = "present_location", joinColumns = @JoinColumn(name = "present_name"), inverseJoinColumns = @JoinColumn(name = "location_name"))
	private List<Location> locations = new ArrayList<Location>();

	public Present() {
	}

	public Present(String name, String description, String pictureId, TargetAutiance targetAudiance,
			PresentType presentType) {
		this.name = name;
		this.description = description;
		this.pictureId = pictureId;
		this.targetAudiance = targetAudiance;
		this.presentType = presentType;
	}

	public void addLocation(Location location) {
		this.locations.add(location);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPictureId() {
		return pictureId;
	}

	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}

	public TargetAutiance getTargetAudiance() {
		return targetAudiance;
	}

	public void setTargetAudiance(TargetAutiance targetAudiance) {
		this.targetAudiance = targetAudiance;
	}

	public PresentType getPresentType() {
		return presentType;
	}

	public void setPresentType(PresentType presentType) {
		this.presentType = presentType;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

}
