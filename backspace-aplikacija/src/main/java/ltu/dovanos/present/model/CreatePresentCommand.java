package ltu.dovanos.present.model;

public class CreatePresentCommand {
	private String name;
	private String description;
	private String pictureId;
	private TargetAutiance targetAudiance;
	private PresentType presentType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPictureId() {
		return pictureId;
	}

	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}

	public TargetAutiance getTargetAudiance() {
		return targetAudiance;
	}

	public void setTargetAudiance(TargetAutiance targetAudiance) {
		this.targetAudiance = targetAudiance;
	}

	public PresentType getPresentType() {
		return presentType;
	}

	public void setPresentType(PresentType presentType) {
		this.presentType = presentType;
	}

}
