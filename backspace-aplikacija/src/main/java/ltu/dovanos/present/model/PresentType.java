package ltu.dovanos.present.model;

public enum PresentType {
	TOY, EVENT, ANIMAL
}
