package ltu.dovanos.present.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import ltu.dovanos.present.Service.PresentData;
import ltu.dovanos.present.Service.PresentService;
import ltu.dovanos.present.model.CreatePresentCommand;

@RestController
@RequestMapping("/api/present")
public class PresentController {

	@Autowired
	private PresentService presentService;

	@PostMapping
	@ApiOperation(value = "createPresent")
	public void createPresent(@RequestBody CreatePresentCommand command) {
		this.presentService.createPresent(new PresentData(command.getName(), command.getDescription(),
				command.getPictureId(), command.getTargetAudiance(), command.getPresentType()));
	}

	@GetMapping
	@ApiOperation(value = "getAllPresents")
	public List<PresentData> getAllPresents() {
		return this.presentService.getAllPresents();
	}

	@RequestMapping(path = "/{presentName}", method = RequestMethod.GET)
	@ApiOperation(value = "getSinglePresent")
	public PresentData getAllPresents(@PathVariable String presentName) {
		return this.presentService.getSinglePresent(presentName);
	}

	@RequestMapping(path = "/{presentName}", method = RequestMethod.PUT)
	@ApiOperation(value = "updatePresent")
	public void updatePresent(@PathVariable String presentName, @RequestBody CreatePresentCommand command) {
		this.presentService.updatePresent(new PresentData(command.getName(), command.getDescription(),
				command.getPictureId(), command.getTargetAudiance(), command.getPresentType()), presentName);
	}

	@RequestMapping(path = "/{presentName}/{locationName}", method = RequestMethod.POST)
	public void addAddress(@PathVariable("presentName") String presentName,
			@PathVariable("locationName") String locationName) {
		this.presentService.addPresentsLocation(locationName, presentName);
	}

}
